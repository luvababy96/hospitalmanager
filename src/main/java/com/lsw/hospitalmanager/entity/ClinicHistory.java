package com.lsw.hospitalmanager.entity;

import com.lsw.hospitalmanager.enums.MedicalItem;
import com.lsw.hospitalmanager.interfaces.CommonModelBuilder;
import com.lsw.hospitalmanager.model.HistoryMedicalItemUpdateRequest;
import com.lsw.hospitalmanager.model.HistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalCustomerId", nullable = false)
    private HospitalCustomer hospitalCustomer;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Boolean isSalary;

    @Column(nullable = false)
    private LocalDate dateCure;

    @Column(nullable = false)
    private LocalTime timeCure;

    @Column(nullable = false)
    private Boolean isCalculate;

    public void putCompletePay() { //U에 해당하는 builder
        this.isCalculate = true;
    }

    public void putMedicalItem(HistoryMedicalItemUpdateRequest updateRequest) {
        this.medicalItem = updateRequest.getMedicalItem();
    }


    private ClinicHistory(ClinicHistoryBuilder builder){  // C에 해당하는 builder
        this.hospitalCustomer = builder.hospitalCustomer;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }

    public static class ClinicHistoryBuilder implements CommonModelBuilder<ClinicHistory> {
        private final HospitalCustomer hospitalCustomer;
        private final MedicalItem medicalItem;
        private final Double price;
        private final Boolean isSalary;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final Boolean isCalculate;

        public ClinicHistoryBuilder(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
            this.hospitalCustomer = hospitalCustomer;
            this.medicalItem = historyRequest.getMedicalItem();
            this.price = historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice();
            this.isSalary = historyRequest.getIsSalary();
            this.dateCure = LocalDate.now();
            this.timeCure = LocalTime.now();
            this.isCalculate = false;

        }

        @Override
        public ClinicHistory build() {
            return new ClinicHistory(this);
        }
    }
}
