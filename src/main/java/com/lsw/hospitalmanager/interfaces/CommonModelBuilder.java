package com.lsw.hospitalmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
