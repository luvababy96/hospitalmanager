package com.lsw.hospitalmanager.repository;

import com.lsw.hospitalmanager.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
