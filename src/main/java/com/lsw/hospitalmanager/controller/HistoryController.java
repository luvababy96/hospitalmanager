package com.lsw.hospitalmanager.controller;

import com.lsw.hospitalmanager.entity.HospitalCustomer;
import com.lsw.hospitalmanager.model.*;
import com.lsw.hospitalmanager.service.CustomerService;
import com.lsw.hospitalmanager.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final CustomerService customerService;
    private final HistoryService historyService;

    @PostMapping("/new/customer-id/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer, request);

        return "OK";
    }

    @GetMapping("/all/date")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);
    }

    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByInsurance(searchDate);
    }

    @GetMapping("/detail/history-id/{historyId}")
    public HistoryResponse getHistory(@PathVariable long historyId) {
        return historyService.getHistory(historyId);
    }

    @PutMapping("/pay-complete/history-id/{historyId}")
    public String putHistoryByPayComplete(@PathVariable long historyId) {
        historyService.putHistoryByPayComplete(historyId);

        return "OK";
    }

    @PutMapping("/clinic-update/history-id/{historyId}")
    public String putHistoryByMedicalItem(@PathVariable long historyId, @RequestBody @Valid HistoryMedicalItemUpdateRequest updateRequest) {
        historyService.putHistoryByMedicalItem(historyId,updateRequest);

        return "OK";
    }
















}
