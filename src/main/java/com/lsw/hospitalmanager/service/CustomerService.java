package com.lsw.hospitalmanager.service;

import com.lsw.hospitalmanager.entity.HospitalCustomer;
import com.lsw.hospitalmanager.model.CustomerRequest;
import com.lsw.hospitalmanager.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();
        hospitalCustomerRepository.save(addData);
    }

    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }
}
